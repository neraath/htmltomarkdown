﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HtmlToPlaintext.Tests
{
    [TestClass]
    public class ContentTests
    {
        [TestMethod]
        public void TestPlaintextContentReturnsOriginalContent()
        {
            var plainText = "This is plain text content.";
            var content = new Content(plainText);
            Assert.AreEqual(plainText, content.OriginalContent);
        }

        [TestMethod]
        public void TestPlaintextContentReturnsOriginalContentWithoutHtml()
        {
            var plainText = "This is some **other** plaintext content! - Wow!";
            var content = new Content(plainText);
            Assert.AreEqual(plainText, content.ContentWithoutHtml);
        }

        [TestMethod]
        public void TestBasicHtmlContentReturnsOriginalContent()
        {
            var basicHtml = "<p>This is a paragraph.</p>";
            var content = new Content(basicHtml);
            Assert.AreEqual(basicHtml, content.OriginalContent);
        }

        [TestMethod]
        public void TestBasicHtmlRemovesHtmlWhenAskingForContentWithoutHtml()
        {
            var basicHtml = "<p>This is a paragraph.</p>";
            var expected = "This is a paragraph.";
            var content = new Content(basicHtml);
            Assert.AreEqual(expected, content.ContentWithoutHtml);
        }

        [TestMethod]
        public void TestIntermediateHtmlRemovesHtmlWhenAskingForContentWithoutHtml()
        {
            var intermediateHtml =
                "<div><h1>This is a header</h1><h2 class=\"pull-right\">This is a subheader</h2><p>This is a <strong>paragraph</strong> - <em>doh!</em></p></div>";
            var converter = new HtmlToMarkdownConverter();
            var converted = converter.Convert(intermediateHtml);
            Assert.IsTrue(converted.Contains("# This is a header"));
            Assert.IsTrue(converted.Contains("## This is a subheader"));
            Assert.IsTrue(converted.Contains("This is a **paragraph** - *doh!*"));
        }

        [TestMethod]
        public void TestHtmlWithHyperlinksRetainsHyperlinks()
        {
            var htmlWithLinks = "<a href=\"http://www.chrisweldon.net\">Chris Weldon's Blog</a>";
            var converter = new HtmlToMarkdownConverter();
            var converted = converter.Convert(htmlWithLinks);
            Assert.IsTrue(converted.Contains("[Chris Weldon's Blog](http://www.chrisweldon.net)"));
        }

        [TestMethod]
        public void TestHtmlWithImagesRetainsImageUrl()
        {
            var htmlWithImage = "<img src=\"http://www.chrisweldon.net/assets/chrisweldon.png\" alt=\"Chris Weldon\" />";
            var converter = new HtmlToMarkdownConverter();
            var converted = converter.Convert(htmlWithImage);
            Assert.IsTrue(converted.Contains("![Chris Weldon](http://www.chrisweldon.net/assets/chrisweldon.png"));
        }

        [TestMethod]
        public void TestHtmlWithNestedParagraphContainingImageAndLinkWillGenerateProperMarkdown()
        {
            var bulkyHtml = @"
<html>
  <body>
    <h1>This is a header</h1>
    <p>
      I will be <strong>extremely</strong> pumped if this works correctly. 
    </p>
    <p>
      Check out <a href='http://www.chrisweldon.net'>Chris Weldon's Blog</a> when you have a chance.
      His picture is pretty dorky: <img src='http://www.chrisweldon.net/assets.chrisweldon.png' alt='Chris Weldon' />
    </p>
  </body>
</html>
";
            var converter = new HtmlToMarkdownConverter();
            var converted = converter.Convert(bulkyHtml);
            Assert.IsTrue(converted.Contains("# This is a header"));
            Assert.IsTrue(converted.Contains("I will be **extremely** pumped if this works correctly."));
            Assert.IsTrue(converted.Contains("Check out [Chris Weldon's Blog](http://www.chrisweldon.net) when you have a chance."));
            Assert.IsTrue(converted.Contains("His picture is pretty dorky: ![Chris Weldon](http://www.chrisweldon.net/assets.chrisweldon.png)"));
        }
    }
}
