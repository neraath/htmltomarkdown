﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace HtmlToPlaintext
{
    public class Content
    {
        private readonly string originalContent;

        public Content(string content)
        {
            originalContent = content;
        }

        public string OriginalContent { get { return originalContent; }}

        public string ContentWithoutHtml
        {
            get
            {
                var htmlObj = new HtmlAgilityPack.HtmlDocument();
                htmlObj.LoadHtml(originalContent);
                string plainText = string.Empty;
                foreach (HtmlNode node in htmlObj.DocumentNode.SelectNodes("//text()"))
                {
                    plainText += node.InnerText;
                }
                return plainText;
            }
        }
    }
}
